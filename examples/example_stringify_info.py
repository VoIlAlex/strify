from strify import stringifyable, StringifyInfo


def format_proper_name(string):
    return ' '.join((word[0].upper() + word[1:].lower() for word in string.split(' ')))


if __name__ == '__main__':
    @stringifyable([
        StringifyInfo('artist', 'artist', format_proper_name),
        StringifyInfo('title', 'title', format_proper_name),
        StringifyInfo('year', 'year'),
        StringifyInfo('hash', '__hash__'),
        StringifyInfo('album')
    ])
    class Song:
        def __init__(self, artist, title, year, album):
            self.artist = artist
            self.title = title
            self.year = year
            self.album = album

        def __hash__(self):
            return hash(str(self.artist) + self.title)

    people = [
        Song(
            artist='we butter the bread with butter',
            title='alptraum song',
            year=2010,
            album='Der Tag an dem die Welt unterging'
        ),
        Song(
            artist='falling in reverse',
            title='loosing my mind',
            year=2018,
            album='Losing My Mind'
        )
    ]

    patterns = [
        '[artist] -- [title] ([year])',
        '[title]: [artist], [year]',
        '[title] ([album])'
    ]

    for person in people:
        for pattern in patterns:
            print(person.stringify(pattern))
